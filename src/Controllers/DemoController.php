<?php
	
	namespace Sixnapps\RecaptchaBundle\Controllers;
	
	use Sixnapps\RecaptchaBundle\Form\Type\SixnappsRecaptchaType;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	
	class DemoController extends AbstractController
	{
		public function show( Request $request )
		{
			$defaultData = [];
			$form        = $this->createFormBuilder( $defaultData )
								->add( 'name', TextType::class )
								->add( 'email', EmailType::class )
								->add( 'message', TextareaType::class )
								->add( 'send', SubmitType::class )
								->add( 'recaptcha', SixnappsRecaptchaType::class )
								->getForm()
			;
			
			$form->handleRequest( $request );
			
			if ( $form->isSubmitted() && $form->isValid() ) {
				// data is an array with "name", "email", and "message" keys
				$data = $form->getData();
				return new Response('Formulaire validé');
			}
			
			return $this->render( '@SixnappsRecaptcha/show.html.twig', [
				'form' => $form->createView(),
			] );
			
		}
	}
