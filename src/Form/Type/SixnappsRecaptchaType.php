<?php
	
	namespace Sixnapps\RecaptchaBundle\Form\Type;
	
	use Sixnapps\RecaptchaBundle\Locale\LocaleResolver;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\FormInterface;
	use Symfony\Component\Form\FormView;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	
	/**
	 * A field for entering a recaptcha text.
	 */
	class SixnappsRecaptchaType extends AbstractType
	{
		/**
		 * The reCAPTCHA server URL.
		 *
		 * @var string
		 */
		protected $recaptchaApiServer;
		
		/**
		 * The reCAPTCHA JS server URL.
		 *
		 * @var string
		 */
		protected $recaptchaApiJsServer;
		/**
		 * The public key.
		 *
		 * @var string
		 */
		protected $publicKey;
		/**
		 * The API server host name.
		 *
		 * @var string
		 */
		protected $apiHost;
		/**
		 * Enable recaptcha?
		 *
		 * @var bool
		 */
		protected $enabled;
		/**
		 * Use AJAX api?
		 *
		 * @var bool
		 */
		protected $ajax;
		/**
		 * @var LocaleResolver
		 */
		protected $localeResolver;
		
		
		/**
		 * @param string         $publicKey Recaptcha public key
		 * @param bool           $enabled   Recaptcha status
		 * @param bool           $ajax      Ajax status
		 * @param LocaleResolver $localeResolver
		 */
		public function __construct( $publicKey, $enabled, $ajax, LocaleResolver $localeResolver,
									 $apiHost = 'www.google.com' )
		{
			$this->publicKey            = $publicKey;
			$this->enabled              = $enabled;
			$this->ajax                 = $ajax;
			$this->apiHost              = $apiHost;
			$this->localeResolver       = $localeResolver;
			$this->recaptchaApiJsServer = sprintf( '//%s/recaptcha/api/js/recaptcha_ajax.js', $apiHost );
			$this->recaptchaApiServer   = sprintf( 'https://%s/recaptcha/api.js', $apiHost );
		}
		
		
		/**
		 * {@inheritdoc}
		 */
		public function buildView( FormView $view, FormInterface $form, array $options )
		{
			$view->vars = array_replace( $view->vars, [
				'sixnapps_recaptcha_enabled' => $this->enabled,
				'sixnapps_recaptcha_ajax'    => $this->ajax,
				'sixnapps_recaptcha_apihost' => $this->apiHost,
			] );
			if ( !$this->enabled ) {
				return;
			}
			if ( !isset( $options[ 'language' ] ) ) {
				$options[ 'language' ] = $this->localeResolver->resolve();
			}
			if ( !$this->ajax ) {
				$view->vars = array_replace( $view->vars, [
					'url_challenge' => sprintf( '%s?hl=%s', $this->recaptchaApiServer, $options[ 'language' ] ),
					'public_key'    => $this->publicKey,
				] );
			} else {
				$view->vars = array_replace( $view->vars, [
					'url_api'    => $this->recaptchaApiJsServer,
					'public_key' => $this->publicKey,
				] );
			}
		}
		
		
		/**
		 * {@inheritdoc}
		 */
		public function configureOptions( OptionsResolver $resolver )
		{
			$resolver->setDefaults( [
				'compound'      => FALSE,
				'language'      => $this->localeResolver->resolve(),
				'public_key'    => NULL,
				'url_challenge' => NULL,
				'url_noscript'  => NULL,
				'attr'          => [
					'options' => [
						'theme'           => 'light',
						'type'            => 'image',
						'size'            => 'normal',
						'callback'        => NULL,
						'expiredCallback' => NULL,
						'bind'            => NULL,
						'defer'           => FALSE,
						'async'           => FALSE,
						'badge'           => NULL,
					],
				],
			] );
		}
		
		
		/**
		 * {@inheritdoc}
		 */
		public function getParent()
		{
			return TextType::class;
		}
		
		
		/**
		 * {@inheritdoc}
		 */
		public function getBlockPrefix()
		{
			return 'sixnapps_recaptcha';
		}
		
		
		/**
		 * Gets the Javascript source URLs.
		 *
		 * @param string $key The script name
		 *
		 * @return string The javascript source URL
		 */
		public function getScriptURL( $key )
		{
			return isset( $this->scripts[ $key ] ) ? $this->scripts[ $key ] : NULL;
		}
		
		
		/**
		 * Gets the public key.
		 *
		 * @return string The javascript source URL
		 */
		public function getPublicKey()
		{
			return $this->publicKey;
		}
		
		
		/**
		 * Gets the API host name.
		 *
		 * @return string The hostname for API
		 */
		public function getApiHost()
		{
			return $this->apiHost;
		}
	}
